# dummy

<!-- 
dummy:
    memungkinkan user untuk melakukan proses booking kereta dengan menyediakan api untuk mendapatkan jadwal, menampilkan promo, melakukan pemesanan, serta pembayaran menggunakan promo ataupun tidak. Selain itu user yang berperan sebagai admin juga dapat melakukan penambahan, perubahan, ataupun peniadaan promo yang ada.

specification dummy:
- using flask for api
- using mysqlalchemy for connection to database (schema name = dummy)
- using jsonschema for validation input
- using jwt for authorization

available api:
- /get-schedule (POST) : mendapatkan jadwal keberangkatan sesuai asal, tujuan, dan tanggal berangkat
    param : 
        {
            "origin" (string | required),
            "destination" (string | required),
            "dep_date" (string date yyyy-mm-dd format | required)
        }

- /get-promo (GET) : mendapatkan keseluruhan promo yang ada
    
- /booking (POST) : melakukan pemesanan sesuai dengan jadwal keberangkatan yang dipilih
    param : 
        {
            "origin" (string | required),
            "destination" (string | required),
            "dep_date" (string date yyyy-mm-dd format | required),
            "caller_name" (string | required),
            "caller_email" (string | required),
            "caller_phone" (string | required),
            "passenger" (object | required) : {
                "pax_name" (array of string | required),
                "pax_identity (array of string | required)"
            }
        }

- /get-book-info (POST) : mendapatkan informasi dari kode booking yang sudah di pesan
    param : 
        {
            "book_code" (string | required)
        }

- /payment (POST) : melakukan pembayaran terhadap pemesanan yang dilakukan
    param : 
        {
            "book_code" (string | required),
            "amount" (integer | required),
            "promo_code" (string | optional)
        }

- /request-token (POST) : mendapatkan token untuk authorization
    param :
        {
            "username" (string | required),
            "password" (string | required)
        }

- /insert-promo (POST) : menambahkan data promo 
    param : 
        {
            "code" (string | required),
            "name" (string | required),
            "amount" (integer | required)
        }

- /update-promo (POST) : mengubah data promo
    param :
        {
            "code" (string | required),
            "name" (string | optional),
            "amount" (integer | optional)
        }

- /delete-promo (POST) : menghapus data promo
    param :
        {
            "code" (string | required)
        }

databases : 
- admin (
        id int PK NN AI, 
        username varchar(100) NN, 
        password varchar(100) NN
    )
- schedule (
        id int PK NN AI,
        origin varchar(45) NN,
        destination varchar(45) NN,
        dep_date datetime NN
    )
- fare (
        id int PK NN AI,
        schedule_id int NN,
        amount int NN
    )
- booking (
        id int PK NN AI,
        schedule_id int NN,
        book_code varchar(6) NN UQ,
        caller_name varchar(100) NN,
        caller_email varchar(100) NN,
        caller_phone varchar(20) NN
    )
- passenger (
        id int PK NN AI,
        booking_id int NN,
        pax_name varchar(100) NN,
        pax_identity varchar(45) NN 
    )
- transaction (
        id int PK NN AI, 
        booking_id int NN,
        promo_id int,
        amount int NN
    )
- promo (
        id int PK NN AI,
        code varchar(45) NN UQ,
        name varchar(45) NN,
        amount int NN
    )

Default data
INSERT INTO schedule (origin, destination, dep_date)
VALUES
	('Gambir', 'Bandung', '2021-09-09 12:00:00'),
    ('Gambir', 'Bandung', '2021-09-10 12:00:00'),
    ('Gambir', 'Bandung', '2021-09-11 12:00:00'),
    ('Gambir', 'Bandung', '2021-09-12 12:00:00'),
    ('Gambir', 'Bandung', '2021-09-13 12:00:00'),
    ('Gambir', 'Bandung', '2021-09-14 12:00:00'),
    ('Gambir', 'Bandung', '2021-09-15 12:00:00'),
    ('Gambir', 'Bandung', '2021-09-16 12:00:00');
        
INSERT INTO fare (schedule_id, amount)
VALUES 
	(1, 100000),
    (2, 100000),
    (3, 100000),
    (4, 100000),
    (5, 100000),
    (6, 100000),
    (7, 100000),
    (8, 100000);
    
INSERT INTO promo (code, name, amount)
VALUES
	('SANBERCODE', 'Promo khusus anggota Sanbercode', 100000),
    ('CREATOR', 'Promo khusus untuk Creator', 10000); 
    
INSERT INTO admin (username, password)
VALUES 
	('admin', 'admin');    

notes : 
    * untuk schedule dan fare hanya bisa diinput hardcode (dianggap hanya bisa add, update, delete lewat app)
    * untuk add update delete hanya bisa dilakukan oleh admin (memerlukan JWT AUTH) 
    * asumsi data dalam table tidak ada yang sama
    
-->