from sqlalchemy.engine import create_engine
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy import func
from sqlalchemy.sql.elements import and_
from sqlalchemy.sql.functions import coalesce

from models.schedule import Schedule
from models.fare import Fare
from models.promo import Promo
from models.booking import Booking
from models.passenger import Passenger
from models.transaction import Transaction

import string
import random

from flask_jwt_extended import *

engine = create_engine('mysql+mysqlconnector://root:000000@localhost:3306/dummy', echo = True)
Session = sessionmaker(bind = engine)
session = Session()

class Transactions:
    def booking(param):
        if len(param['passenger']['pax_name']) != len(param['passenger']['pax_identity']):
            return {
                "message" : "Total pax_name and total pax_identity must be the same!"
            }

        checkBookCode = 1

        try:
            schedule = session.query(Schedule).filter(
                and_(
                    Schedule.origin == param['origin'],
                    Schedule.destination == param['destination'],
                    func.date(Schedule.dep_date) == param['dep_date']
                )
            ).one()
        except Exception as e:
            return {
                "message" : "Schedule not found!"
            }

        while checkBookCode:
            bookCode = ''.join(random.choices(string.ascii_uppercase + string.digits, k=6))
            checkBookCode = len(session.query(Booking).filter(Booking.book_code == bookCode).all())

        booking = {
            "schedule_id" : schedule.id,
            "book_code" : bookCode,
            "caller_name" : param['caller_name'],
            "caller_email" : param['caller_email'],
            "caller_phone" : param['caller_phone']
        }

        session.add(Booking(**booking))
        session.commit()

        try:
            bookingId = session.query(Booking.id).filter(Booking.book_code == bookCode).one()
        except Exception as e:
            return {
                "message" : "Booking not found!"
            }

        for i in range(0, len(param['passenger']['pax_name'])):
            pax = {
                "booking_id" : bookingId[0],
                "pax_name" : param['passenger']['pax_name'][i],
                "pax_identity" : param['passenger']['pax_identity'][i]
            }
            session.add(Passenger(**pax))
            session.commit()

        return {
            "book_code" : bookCode,
            "message" : "Insert Booking Success"
        }

    def payment(param):
        try:
            booking = session.query(
                Booking.id,
                Fare.amount
            ).filter(
                and_(
                    Booking.book_code == param['book_code'],
                    Fare.schedule_id == Booking.schedule_id
                )
            ).one()
        except Exception as e:
            return {
                "message" : "Book code not found!"
            }

        booking = dict(zip([
            'booking_id', 'fare'
        ], booking))

        transaction = session.query(Transaction).filter(Transaction.booking_id == booking['booking_id']).all()

        if len(transaction) > 0:
            return {
                "message" : "Book code {} already paid".format(param['book_code'])
            }

        if param.get('promo_code') is not None and param.get('promo_code') != '':
            try:
                promo = session.query(Promo).filter(Promo.code == param['promo_code']).one()
                fare = booking['fare'] - int(promo.amount)
                promo_id = promo.id
            except Exception as e:
                return {
                    "message" : "Promo not found!"
                }
        else:
            fare = booking['fare']
            promo_id = None

        if fare != int(param['amount']):
            return {
                "message" : "Total Fare and total payment is not the same!"
            }
        else:
            temp = {
                "booking_id" : booking['booking_id'],
                "amount" : param['amount'],
                "promo_id" : promo_id
            }
            session.add(Transaction(**temp))
            session.commit()

            return {
                "message" : "Payment with book code {} success!".format(param['book_code'])
            }

    @jwt_required()
    def insertPromo(param):
        promo = session.query(Promo).filter(Promo.code == param['code']).all()

        if len(promo) > 0:
            return {
                "message" : "Promo already exists!"
            }

        if param['code'] is None or param['code'] == '':
            return {
                "message" : "Promo code is empty!"
            }

        if param['name'] is None or param['name'] == '':
            return {
                "message" : "Promo name is empty!"
            }

        if param['amount'] is None or param['amount'] == 0:
            return {
                "message" : "Promo amount is empty!"
            }

        newPromo = session.add(Promo(**param))
        session.commit()

        return {
            "message" : "Promo with code {} successfully inserted".format(param['code'])
        }

    @jwt_required()
    def updatePromo(param):
        try:
            promo = session.query(Promo).filter(Promo.code == param['code']).one()
        except Exception as e:
            return {
                "message" : "Promo code not found"
            }

        if param['name'] is not None and param['name'] != '':
            promo.name = param['name']
            session.commit()

        if param['amount'] is not None and param['amount'] != 0:
            promo.amount = param['amount']
            session.commit()

        return {
            "message" : "Promo with code {} successfully updated".format(param['code'])
        }

    @jwt_required()
    def deletePromo(param):
        try:
            promo = session.query(Promo).filter(Promo.code == param['code']).one()
        except Exception as e:
            return {
                "message" : "Promo code not found"
            }

        session.delete(promo)
        session.commit()

        return {
            "message" : "Promo with code {} successfully deleted".format(param['code'])
        }