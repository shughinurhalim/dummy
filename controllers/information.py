from sqlalchemy.engine import create_engine
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy import func
from sqlalchemy.sql.elements import and_
from sqlalchemy.sql.functions import coalesce

from models.schedule import Schedule
from models.fare import Fare
from models.promo import Promo
from models.booking import Booking
from models.passenger import Passenger
from models.transaction import Transaction
from models.admin import Admin

from flask_jwt_extended import *
import datetime

engine = create_engine('mysql+mysqlconnector://root:000000@localhost:3306/dummy', echo = True, isolation_level="READ UNCOMMITTED")
Session = sessionmaker(bind = engine)
session = Session()

class Informations:
    def getSchedule(param):
        schedule = session.query(
                Schedule.origin,
                Schedule.destination,
                func.date_format(Schedule.dep_date, '%d-%m-%Y %H:%i:%s'), 
                Fare.amount
            ).filter(
                and_(
                    Schedule.id == Fare.schedule_id,
                    Schedule.origin == param['origin'],
                    Schedule.destination == param['destination'],
                    func.date(Schedule.dep_date) == param['dep_date']
                )
            ).all()

        if len(schedule) == 0:
            return {
                "Schedule" : "No schedule found!"
            }

        result = []

        for data in schedule:
            temp = {
                "Origin" : data[0],
                "Destination" : data[1],
                "Departure_Date" : data[2],
                "Fare" : data[3]
            }
            result.append(temp)

        return {
            "Schedule" : result
        }

    def getPromo():
        promo = session.query(
                Promo.code,
                Promo.name,
                Promo.amount
            ).all()

        if len(promo) == 0:
            return {
                "Promo" : "No promo found"
            }

        result = []

        for data in promo:
            temp = {
                "Promo_Code" : data[0],
                "Promo_Name" : data[1],
                "Promo_Amount" : data[2]
            }
            result.append(temp)

        return {
            "Promo" : result
        }

    def getBookInfo(param):
        try:
            booking = session.query(
                Schedule.origin,
                Schedule.destination,
                func.date_format(Schedule.dep_date, '%d-%m-%Y %H:%i:%s'), 
                Fare.amount,
                Booking.book_code,
                Booking.caller_name,
                Booking.caller_email,
                Booking.caller_phone,
                coalesce(Transaction.amount, 0),
                coalesce(Promo.amount, 0),
                Booking.id
            ).outerjoin(
                Transaction, 
                Transaction.booking_id == Booking.id
            ).outerjoin(
                Promo,
                Promo.id == Transaction.promo_id
            ).filter(
                and_(
                    Booking.schedule_id == Schedule.id,
                    Booking.book_code == param['book_code'],
                    Schedule.id == Fare.schedule_id
                )
            ).one()
        except Exception as e:
            return {
                "message" : "Book code not found!"
            }

        print(booking)

        booking = dict(zip([
            'origin', 'destination', 'dep_date', 'fare', 'book_code', 'caller_name', 'caller_email', 'caller_phone', 'payment', 'discount', 'booking_id'
        ], booking))

        pax = session.query(Passenger).filter(Passenger.booking_id == booking['booking_id']).all()
        paxList = []

        for data in pax:
            paxList.append({
                "pax_name" : data.pax_name,
                "pax_identity" : data.pax_identity
            })

        return {
            "origin" : booking['origin'],
            "destination" : booking['destination'],
            "departure_date" : booking['dep_date'],
            "fare" : booking['fare'],
            "book_code" : booking['book_code'],
            "caller_name" : booking['caller_name'],
            "caller_email" : booking['caller_email'],
            "caller_phone" : booking['caller_phone'],
            "payment" : int(booking['payment']),
            "discount" : int(booking['discount']),
            "passenger" : paxList
        }

    def getToken(param):
        try:
            admin = session.query(Admin).filter(
                and_(
                    Admin.username == param['username'],
                    Admin.password == param['password']
                )
            ).one()
        except Exception as e:
            return {
                "message" : "Admin not found!"
            }

        expires = datetime.timedelta(days=1)
        access_token = create_access_token({"username" : param['username']}, fresh=True, expires_delta=expires)

        return {
            "username" : param['username'],
            "access_token" : access_token
        }