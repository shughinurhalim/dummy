from flask import Flask, jsonify, request
from flask_jwt_extended import JWTManager

from jsonschema import validate, ValidationError, FormatChecker
from jsonschema.exceptions import SchemaError
from controllers.information import Informations
from controllers.transaction import Transactions

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False
app.config['JWT_SECRET_KEY'] = "somesecretkey"
jwt = JWTManager(app)

schema = {
    "get-schedule" : {
        "type" : "object",
        "properties" : {
            "origin" : {
                "type" : "string",
                "minLength" : 1,
                "maxLength" : 45
            },
            "destination" : {
                "type" : "string",
                "minLength" : 1,
                "maxLength" : 45
            },
            "dep_date" : {
                "type" : "string",
                "format" : "date"
            }
        },
        "required" : ["origin", "destination", "dep_date"]
    },
    "booking" : {
        "type" : "object",
        "properties" : {
            "origin" : {
                "type" : "string",
                "minLength" : 1,
                "maxLength" : 45
            },
            "destination" : {
                "type" : "string",
                "minLength" : 1,
                "maxLength" : 45
            },
            "dep_date" : {
                "type" : "string",
                "format" : "date"
            },
            "caller_name" : {
                "type" : "string",
                "minLength" : 1,
                "maxLength" : 100
            },
            "caller_phone" : {
                "type" : "string",
                "pattern" : "[0-9]+",
                "minLength" : 1,
                "maxLength" : 20
            },
            "caller_email" : {
                "type" : "string",
                "format" : "email",
                "minLength" : 1,
                "maxLength" : 100
            },
            "passenger" : {
                "type" : "object",
                "properties" : {
                    "pax_name" : {
                        "type" : "array",
                        "items" : [
                            {
                                "type" : "string",
                                "minLength" : 1,
                                "maxLength" : 100
                            }
                        ],
                        "minItems" : 1
                    },
                    "pax_identity" : {
                        "type" : "array",
                        "items" : [
                            {
                                "type" : "string",
                                "pattern" : "[0-9]+",
                                "minLength" : 1,
                                "maxLength" : 45
                            }
                        ],
                        "minItems" : 1
                    }
                },
                "required" : ["pax_name", "pax_identity"]
            }
        },
        "required" : ["origin", "destination", "dep_date", "caller_name", "caller_phone", "caller_email", "passenger"]
    },
    "get-book-info" : {
        "type" : "object",
        "properties" : {
            "book_code" : {
                "type" : "string",
                "minLength" : 1,
                "maxLength" : 6
            }
        },
        "required" : ["book_code"]
    },
    "payment" : {
        "type" : "object",
        "properties" : {
            "book_code" : {
                "type" : "string",
                "minLength" : 1,
                "maxLength" : 6
            },
            "amount" : {
                "type" : "integer"
            },
            "promo_code" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 45
            }
        },
        "required" : ["book_code", "amount"]
    },
    "request-token" : {
        "type" : "object",
        "properties" : {
            "username" : {
                "type" : "string",
                "minLength" : 1,
                "maxLength" : 100
            },
            "password" : {
                "type" : "string",
                "minLength" : 1,
                "maxLength" : 100
            }
        },
        "required" : ["username", "password"]
    },
    "insert-promo" : {
        "type" : "object",
        "properties" : {
            "code" : {
                "type" : "string",
                "minLength" : 1,
                "maxLength" : 45
            },
            "name" : {
                "type" : "string",
                "minLength" : 1,
                "maxLength" : 45
            },
            "amount" : {
                "type" : "integer"
            }
        },
        "required" : ["code", "name", "amount"]
    },
    "update-promo" : {
        "type" : "object",
        "properties" : {
            "code" : {
                "type" : "string",
                "minLength" : 1,
                "maxLength" : 45
            },
            "name" : {
                "type" : "string",
                "minLength" : 0,
                "maxLength" : 45
            },
            "amount" : {
                "type" : "integer"
            }
        },
        "required" : ["code"]
    },
    "delete-promo" : {
        "type" : "object",
        "properties" : {
            "code" : {
                "type" : "string",
                "minLength" : 1,
                "maxLength" : 45
            }
        },
        "required" : ["code"]
    }
}

@app.route('/')
def main():
    return "Test"

@app.route('/get-schedule', methods=["POST"])
def getSchedule():
    param = request.json

    try:
        validate(param, schema['get-schedule'], format_checker=FormatChecker())
    except ValidationError as e:
        return jsonify({
            "Validation_Error" : e.message
        })
    except Exception as e:
        return jsonify({
            "Error" : e
        })

    return jsonify(Informations.getSchedule(param))

@app.route('/get-promo')
def getPromo():
    return jsonify(Informations.getPromo())

@app.route('/booking', methods=['POST'])
def booking():
    param = request.json

    try:
        validate(param, schema['booking'], format_checker=FormatChecker())
    except ValidationError as e:
        if e.schema_path[-1] == 'pattern':
            # get property name 
            e.schema_path.reverse()
            i = e.schema_path.index('properties')
            e.schema_path.reverse()

            return jsonify({
                "Validation_Error" : "{} is not valid pattern".format(e.schema_path[len(e.schema_path) - i])
            })
        else:
            return jsonify({
                "Validation_Error" : e.message
            })
    except Exception as e:
        return jsonify({
            "Error" : e
        })

    return jsonify(Transactions.booking(param))
    
@app.route('/get-book-info', methods=['POST'])
def getBookInfo():
    param = request.json

    try:
        validate(param, schema['get-book-info'], format_checker=FormatChecker())
    except ValidationError as e:
        return jsonify({
            "Validation_Error" : e.message
        })
    except Exception as e:
        return jsonify({
            "Error" : e
        })

    return jsonify(Informations.getBookInfo(param))

@app.route('/payment', methods=['POST'])
def payment():
    param = request.json

    try:
        validate(param, schema['payment'], format_checker=FormatChecker())
    except ValidationError as e:
        if e.schema_path[-1] == 'pattern':
            # get property name 
            e.schema_path.reverse()
            i = e.schema_path.index('properties')
            e.schema_path.reverse()

            return jsonify({
                "Validation_Error" : "{} is not valid pattern".format(e.schema_path[len(e.schema_path) - i])
            })
        else:
            return jsonify({
                "Validation_Error" : e.message
            })
    except Exception as e:
        return jsonify({
            "Error" : e
        })

    return jsonify(Transactions.payment(param))

@app.route('/request-token', methods=['POST'])
def requestToken():
    param = request.json

    try:
        validate(param, schema['request-token'])
    except ValidationError as e:
        return jsonify({
            "Validation_Error" : e.message
        })
    except Exception as e:
        return jsonify({
            "Error" : e
        })

    return jsonify(Informations.getToken(param))

@app.route('/insert-promo', methods=['POST'])
def insertPromo():
    param = request.json

    try:
        validate(param, schema['insert-promo'])
    except ValidationError as e:
        return jsonify({
            "Validation_Error" : e.message
        })
    except Exception as e:
        return jsonify({
            "Error" : e
        })

    return jsonify(Transactions.insertPromo(param))

@app.route('/update-promo', methods=['POST'])
def updatePromo():
    param = request.json

    try:
        validate(param, schema['update-promo'])
    except ValidationError as e:
        return jsonify({
            "Validation_Error" : e.message
        })
    except Exception as e:
        return jsonify({
            "Error" : e
        })

    return jsonify(Transactions.updatePromo(param))

@app.route('/delete-promo', methods=['POST'])
def deletePromo():
    param = request.json

    try:
        validate(param, schema['delete-promo'])
    except ValidationError as e:
        return jsonify({
            "Validation_Error" : e.message
        })
    except Exception as e:
        return jsonify({
            "Error" : e
        })

    return jsonify(Transactions.deletePromo(param))

if __name__ == "__main__":
    app.run(debug = True)