from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql.schema import Column
from sqlalchemy.sql.sqltypes import Integer, String, DateTime

Base = declarative_base()

class Schedule(Base):
    __tablename__ = 'schedule'
    id = Column(Integer, primary_key = True)
    origin = Column(String)
    destination = Column(String)
    dep_date = Column(DateTime)