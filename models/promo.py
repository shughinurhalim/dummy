from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql.schema import Column
from sqlalchemy.sql.sqltypes import Integer, String

Base = declarative_base()

class Promo(Base):
    __tablename__ = "promo"
    id  = Column(Integer, primary_key=True)
    code = Column(String)
    name = Column(String)
    amount = Column(Integer)