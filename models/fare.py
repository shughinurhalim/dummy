from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql.schema import Column
from sqlalchemy.sql.sqltypes import Integer

Base = declarative_base()

class Fare(Base):
    __tablename__ = 'fare'
    id = Column(Integer, primary_key=True)
    schedule_id = Column(Integer)
    amount = Column(Integer)