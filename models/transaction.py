from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql.schema import Column
from sqlalchemy.sql.sqltypes import Integer

Base = declarative_base()

class Transaction(Base):
    __tablename__ = "transaction"
    id = Column(Integer, primary_key=True)
    booking_id = Column(Integer)
    promo_id = Column(Integer)
    amount = Column(Integer)