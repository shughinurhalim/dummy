from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql.schema import Column
from sqlalchemy.sql.sqltypes import Integer, String

Base = declarative_base()

class Booking(Base):
    __tablename__ = "booking"
    id = Column(Integer, primary_key=True)
    schedule_id = Column(Integer)
    book_code = Column(String)
    caller_name = Column(String)
    caller_phone = Column(String)
    caller_email = Column(String)