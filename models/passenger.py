from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql.schema import Column
from sqlalchemy.sql.sqltypes import Integer, String

Base = declarative_base()

class Passenger(Base):
    __tablename__ = "passenger"
    id = Column(Integer, primary_key=True)
    booking_id = Column(Integer)
    pax_name = Column(String)
    pax_identity = Column(String)